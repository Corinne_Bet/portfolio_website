---
  layout: project
  weigth: 75
  title: "Murmurations"
  year: "2016"
  description: "Un jeu vidéo autour de la thématique du vote"
  important: false
  maker: true
  designer: false
  tags: [Jeu vidéo, Graphisme]
  collaboration: [Pierre Corbinais, Géraldine Delacroix, Brice Dubat, Delphine Fourneau]
  chapeau: "→ Réalisation d'un jeu vidéo autour de la mécanique de la propagation des idées politiques, lors de la Mediajam"
  liens:
   - nom: "Article" 
     lien: "https://www.mediapart.fr/journal/culture-idees/221016/mediajam-decouvrez-nos-huit-jeux-video-citoyens-et-engages?page_article=2"

  imagesFolder: murmurations
  thumbnail: MurmurationsPhoto01.png
  image:
   - url: MurmurationsPhoto02.png
     caption: "Capture d'écran du gameplay du jeu"
     size: 'big'
   - url: MurmurationsSketch01.png
     caption: "Recherches sur le design des personnages. Chaque carré qui les compose représente une idée"
     size: 'small'
   - url: MurmurationsSketch02.png
     caption: "Recherches sur le design des personnages. Chaque carré qui les compose représente une idée"
     size: 'small'
   - url: MurmurationsSketch03.png
     caption: "Concept art d'une scène du jeu avec le principe de place publique comme décor"
     size: 'big'
   - url: MurmurationsPhoto03.png
     caption: "Capture d'écran en jeu"
     size: 'big'
---

En 2017, à l'occasion des élections présidentielles à venir, [Mediapart](https://www.mediapart.fr) et la belle Game ont organisé une gamejam autour de la notion de vote et d'élections.

Chaque équipe était composée de différents profils du jeu vidéo (game designer, développeur, graphiste), associé à un journaliste de médiapart.

Nous avons créé Murmurations, un jeu autour de la notion de diffusion des idées et de leur appauvrissement, dans l'optique de voter pour un parti. Le jeu se découpe en deux parties, une première où le joueur peut diffuser ses idées pour étendre son influence, puis une seconde où le joueur doit traverser le mur, symbolisant l'acte de vote qui va lui faire perdre une partie de ses idées.

Vous pouvez tester le jeu en le téléchargeant à cette [adresse](https://www.mediapart.fr/journal/culture-idees/221016/mediajam-decouvrez-nos-huit-jeux-video-citoyens-et-engages?page_article=2).
