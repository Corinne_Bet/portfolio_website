---
  layout: project
  weight: 65
  title: "Signalétique Fablab Amsterdam"
  year: "2016"
  description: "Prototype de signalétique du Fablab d'amsterdam"
  important: false
  maker: true
  designer: true
  tags: [Graphisme , Design , Fabrication]
  collaboration: []
  chapeau: "→ Dans le cadre de mon stage au Fablab d'amsterdam j'ai prototypé une proposition de signalétique"
  liens: 
  imagesFolder: signaletiqueWaag
  thumbnail: signaletiqueWaagPhoto01.png
  image:
   - url: signaletiqueWaagPhoto02.png
     caption: "Le château de la Waag, où se situe le Fablab"
     size: 'medium'
   - url: signaletiqueWaagPhoto01.png
     caption: "Installation de la signalétique de la fraiseuse, suspendue au-dessus de la machine"
     size: 'big'
   - url: signaletiqueWaagPhoto05.png
     caption: "Le pictogramme de la machine s'inscrit au sein d'un blason héraldique, en allusion au lieu"
     size: 'medium'
   - url: signaletiqueWaagPhoto04.png
     caption: "La signalétique est composée d'une superposition de carton donnant une texture sur la face latérale"
     size: 'medium'
   - url: signaletiqueWaagPhoto03.png
     caption: "Fabrication de la signalétique"
     size: 'medium'
   - url: signaletiqueWaagPhoto06.png
     caption: "Recherches autour des pictogrammes"
     size: 'small'
   - url: signaletiqueWaagPhoto07.png
     caption: "Gravure du pictogramme sur bois"
     size: 'small'
---

Le Fablab d'Amsterdam est situé dans le bâtiment historique de la Waag, château avec ses quatres tours. J'ai décidé de faire une signalétique s'inspirant des symboles héraldiques.

Chaque machine a fait l'objet d'une réflexion sur son dessin. J'ai décidé de me servir exclusivement de carton, couleur naturelle, afin de ne pas surcharger l'espace. Le carton a été recyclé auprès des commerçants du quartier.
