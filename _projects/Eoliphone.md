---
  layout: project
  weight: 90
  title: "Éoliphone"
  year: "2020"
  description: "Une machine pour fabriquer le bruit du vent"
  important: false
  maker: true
  designer: false
  tags: [Son , Fabrication ]
  collaboration: ["Clémence Chaillot"]
  chapeau: "→ Fabrication d'un éoliphone, ou machine à vent, en lien avec l'équipe de médiation des Franciscaines de Deauville."
  liens: ""
  imagesFolder: eoliphone
  thumbnail: EoliphonePhoto01.png
  image:
   - url: EoliphonePhoto01.png
     caption: "L'éoliphone assemblé"
     size: 'big'
   - url: EoliphonePhoto03.png
     caption: "L'éoliphone est composé d'un tambour, constitué de tasseaux en bois qui, lorsqu'ils sont en mouvement, frottent sur le tissu et produisent ainsi le son du vent"
     size: 'small'
   - url: EoliphonePhoto04.png
     caption: ""
     size: 'small'
   - url: EoliphonePhoto02.png
     caption: "Pièces de jonctions imprimées en 3D"
     size: 'medium'
---

Dans le cadre de son atelier autour du son, l'équipe médiation souhaitait proposer une machine qui reproduit le vent, déjà existante.
Ne trouvant pas de plans de la machine, nous avons décidé de fabriquer la notre en utilisant l'établi du Fablab et l'imprimante 3D.
