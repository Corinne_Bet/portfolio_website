---
  layout: project
  weight: 95
  title: "La boîte à odeur"
  year: "2020"
  description: "Une 'boîte' à odeur pour la médiation"
  important: false
  maker: true
  designer: true
  tags: [Médiation , Fabrication, Design]
  collaboration: []
  chapeau: "→ Design et fabrication d'une boîte à odeur, en lien avec l'équipe de médiation des Franciscaines de Deauville"
  liens: "onshape.com"
  imagesFolder: boiteAOdeur
  thumbnail: BatonAOdeurPhoto01.png
  image:
   - url: BatonAOdeurPhoto01.png
     caption: 'La tête dont le moucharabier permet de laisser passer les odeurs'
     size: 'big'
   - url: BatonAOdeurSketch01.png
     caption: 'Esquisses de recherche'
     size: 'medium'
   - url: BatonAOdeurPhoto02.png
     caption: 'Les 3 composants : corps, base et tête'
     size: 'medium'
   - url: BatonAOdeurPhoto03.png
     caption: 'Profil du bâton à odeur'
     size: 'medium'
---

Dans le cadre de ses visites de l'exposition inaugurale des Franciscaines, l'équipe de médiation avait besoin d'un dispositif permettant de faire sentir des odeurs au public.

La réalisation ne devait pas être trop encombrante, afin de permettre aux médiateur·ice·s de les avoir avec eux durant le temps de leurs visites.

Après un premier échange sur le prototype en impression 3D, s'inspirant de la forme d'un entonnoir inversé, j'ai décidé de concevoir un objet plus fin, se manipulant à l'horizontal, chose plus naturelle pour sentir. Aussi, j'ai mêlé impression 3D et bois provenant des chutes d'un précédent projet, pour la fabrication. 

L'objet se divise en 3 parties : le manche en bois, constituant le corps, la base filetée sur lequel vient se visser la dernière partie, la tête, bloc principal dans lequel on glisse les objets odorants. Le tout peut facilement se ranger dans un sac ou une poche.
