---
  layout: project
  weight: 85
  title: "Codeurs à la sauvette"
  year: "2016"
  description: "Ateliers clandestins de vulgarisation de la culture numérique"
  important: true
  maker: true
  designer: true
  tags: [Médiation , Fabrication, Design]
  collaboration: []
  chapeau: "→ Ateliers clandestins de vulgarisation de la culture numérique"
  liens: "onshape.com"
  imagesFolder: codeursALaSauvette
  thumbnail: CodeursALaSauvettePhoto01.png
  image:
   - url: CodeursALaSauvettePhoto01.png
     caption: "Affiches d'annonce du prochain atelier des Codeurs à la sauvette"
     size: 'medium'
   - url: CodeursALaSauvettePhoto02.png
     caption: "Atelier autour de l'encre numérique"
     size: 'medium'
   - url: CodeursALaSauvettePhoto03.png
     caption: "Atelier autour de la VR avec test des casques"
     size: 'medium'
   - url: CodeursALaSauvettePhoto04.png
     caption: "Atelier autour du makey makey"
     size: 'small'
   - url: CodeursALaSauvettePhoto05.png
     caption: "Atelier autour du makey makey"
     size: 'small'
---

Au cours de nos années d'études à l'école Estienne, avec des camarades de classe, nous avons co-animés des ateliers à destination des autres élèves de l'école sur la culture numérique.
