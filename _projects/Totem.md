---
  layout: project
  weight: 70
  title: "Totem"
  year: "2017"
  description: "Un dispositif de partage de projets pour les communautés locales de tiers lieux garantissant l'anonymat de ses usagers"
  important: true
  maker: true
  designer: false
  tags: [Design , Fabrication, Programmation]
  collaboration: [Riwad Salim , Valentin Socha]
  chapeau: "→ Un dispositif de partage de projets à destination de communautés de tiers lieux. Ce projet fait suite à l'écriture d'un mémoire sur la thématique 'designer l'anonymat'"
  liens: 
  imagesFolder: totem
  thumbnail: TotemPhoto06.png
  image:
   - url: TotemPhoto06.png
     caption: "Chaque usager peut générer son signe graphique distinctif. Celui-ci se génére sur une grille 8x8"
     size: 'big'
   - url: TotemPhoto04.png
     caption: "Interface d'édition du dispositif totem"
     size: 'big'
   - url: TotemPhoto01.png
     caption: "Exemple d'installation du dispositif totem"
     size: 'medium'
   - url: TotemPhoto02.png
     caption: "Scénographie de nos recherches autour du projet"
     size: 'medium'
   - url: TotemPhoto07.png
     caption: "Projection de l'installation du dispositif dans le cadre des 'Grands voisins'"
     size: 'medium'
   - url: TotemPhoto08.png
     caption: "Possibilité d'usage de totem avec deux contrôleur et un écran"
     size: 'small'
   - url: TotemPhoto09.png
     caption: "Possibilité d'usage de totem avec un contrôleur mais plusieurs écrans"
     size: 'small'
   - url: TotemPhoto10.png
     caption: "Possibilité d'usage de totem avec une toile et un vidéoprojecteur"
     size: 'small'
   - url: TotemPhoto05.png
     caption: "Logotype de notre groupe 'Corinne B.' réalisé en ASCII"
     size: 'small'
---

À la suite d'un mémoire autour du thème de "Designer l'anonymat" co-écrit avec [Valentin Socha](http://valentinsocha.fr/) et Riwad Salim, nous avons concrétisé nos écrits dans le projet "Totem"

Totem est un dispositif de partage de projets à destination des tiers lieux comme des Fablabs. Ce projet à pour objet l'anonymisation de ses usagers par, entre autre, leur représentation sous la forme d'un symbole graphique unique qui est au coeur de l'expérience utilisateur. Ce dernier permet, au delà de l'identification possible de son possesseur, de laisser sa trace sur l'interface. Cette trace est à un sens définit par la communauté des usagers.

Le desipositif s'incarne sous la forme d'un serveur avec son propre contrôleur mais avec une multitude de scénario d'équipement selon les ressources locales. Afin de mettre en avant la communauté locale, l'édition du contenu de totem ne peut se faire que sur le réseau local et une version "visisteur" sans possibilité d'édition est quant à elle disponible à tous, sur internet.
