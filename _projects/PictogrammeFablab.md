---
  layout: project
  weight: 100
  title: "Pictogrammes Fablab"
  year: "2017"
  description: "Conception d'un jeu de pictogrammes associés au Fablab"
  important: true
  maker: false
  designer: true
  tags: [Design, Graphisme]
  collaboration:
  chapeau: "→ J'ai créée un jeu de pictogrammes afin de mieux identifier les espaces machine du Fablab et dans l'optique de la mise en place de badges"
  liens:
   - nom: Fichiers
     lien: https://thenounproject.com/browse/creator/quentinlaloux/icons/?p=1
  imagesFolder: pictogrammeFablab
  thumbnail: pictogrammeFablab01.png
  image:
   - url: pictogrammeFablab01.png
     caption: "Les pictogrammes sous la forme de tampons réalisés par @Darkbean"
     size: 'big'
   - url: pictogrammeFablab05.png
     caption: "Un panneau de présentation du Fablab la COcotte numérique par @Darkbean"
     size: 'big'
   - url: pictogrammeFablab02.png
     caption: "L'ensemble des pictogrammes disponibles sur le site The Noun Project"
     size: 'big'
   - url: pictogrammeFablab03.png
     caption: "Principe de construction modulaire des badges"
     size: 'big'
   - url: pictogrammeFablab04.png
     caption: "Pictogramme de la conception 3D et de la fraiseuse"
     size: 'big'
---

Suite à mes différents passages dans des makerspaces et Fablab, j'ai fait le constat d'un manque réccurent de signes graphiques permettant d'identifier facilement les machines de ces lieux.

Le Dôme souhaitant mettre en place des Open-Badges, j'ai donc réalisé à cette occasion des îcones à multiple usage. Elles devaient permettre d'identifier les machines sur place, sur les différents supports écrans et sur des Open-Badges.

Est né de cette envie, différentes contraintes : des pictogrammes lisibles à petite échelle (pouvant être glissé dans un CV A4 sous la forme d'Open Badges) ; Des pictogrammes reproductibles avec toutes les machines du Fablab (mise au point d'un travail graphique d'un bloc contenu) ; Des pictogrammes modulaires afin de les associer ensemble pour signifier la multitude des badges possibles.

Ce projet est partagé en open-source.
