---
  layout: project
  weight: 91
  title: "Prix BD Dans ta bulle ! 2024"
  year: "2023"
  description: "Affiche du prix BD Dans ta bulle !, édition 2024"
  important: false
  maker: false
  designer: true
  tags: [Graphisme , Design]
  collaboration: []
  chapeau: "→ J'ai réalisé la nouvelle affiche pour l'édition 2024 du prix BD, Dans Ta Bulle !"
  liens: 
  imagesFolder: dansTaBulle2024
  thumbnail: dansTaBulle202402.png
  image:
   - url: dansTaBulle202401.png
     caption: "L'affiche du prix 2024"
     size: 'medium'
---

Pour cette nouvelle édition du prix, j'ai souhaité explorer une approche plus vernaculaire en intégrant le prix dans la ville d'Argentan. Par ailleurs, j'ai souhaité aborder le titre du prix par son aspect onirique.

Inspiré par des affiches comme celle réalisée par Denis Bajram pour le 26ème Rendez-vous de la bande dessinée d'Amiens, j'ai souhaité faire allusion au patrimoine architectural de la ville, et notamment à son église Saint-Germain. Évocation concrète de la ville que j'ai souhaité contre balancée par cette vision fantastique d'une bulle flottante, une île sereine volant au-dessus de la ville.

