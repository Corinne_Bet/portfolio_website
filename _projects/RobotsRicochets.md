---
  layout: project
  weight: 75
  title: "Robots Ricochets"
  year: "2020"
  description: "Fabrication du jeu Robots Ricochets et de sa console pour un public scolaire"
  important: true
  maker: true
  designer: true
  tags: [Jeu vidéo , Électronique , Fabrication]
  collaboration: []
  chapeau: "→ Robot Ricochet est un jeu vidéo et sa console, dont l'objectif est l'apprentissage des bases de programmation pour du jeune public."
  liens:
   - nom: Jeu à télécharger (tic80)
     lien: https://gitlab.com/Corinne_Bet/robots-ricochets
   - nom: Fichiers 3D
     lien: https://cad.onshape.com/documents/33da00bf24556770e05c2238/w/a0e386386fad2375359602f3/e/d1cbb49cb6842660511bb91d
  imagesFolder: robotsRicochets
  thumbnail: RobotsRiochetsPhoto01.png
  image:
   - url: RobotsRiochetsPhoto01.png
     caption: 'La console de jeu branché à un écran'
     size: 'big'
   - url: RobotsRiochetsPhoto02.png
     caption: 'Les trois personnages du jeu'
     size: 'small'
   - url: RobotsRiocchetsSketch01.png
     caption: 'Esquisses de recherches pour le design de la console'
     size: 'medium'   
   - url: RobotsRiochetsPhoto03.png
     caption: 'Exemple de séquence'
     size: 'small'   
   - url: RobotsRiochetsPhoto05.png
     caption: 'Boîte de rangement de Robots Ricochets'
     size: 'small'   
   - url: RobotsRiochetsPhoto07.png
     caption: 'La console de jeu avec son joystick pour contrôler les personnages, les trois boutons pour sélectionner son personnage et le bouton noir pour lancer la séquence'
     size: 'small'   
   - url: RobotsRiochetsPhoto12.png
     caption: 'Écran des règles du jeu'
     size: 'big'   
   - url: RobotsRiochetsPhoto14.png
     caption: 'Modélisation 3D de la console sur Onshape'
     size: 'big'
---

Ce projet s’incarne, à la fois, dans un jeu vidéo réalisé sur le moteur de jeu Tic 80, et dans une manette de jeu qui est aussi une console complète, basée sur un rapsberry pi et sur l’OS recalbox.

Le jeu vidéo est la transcription du jeu de société Robot Ricochet. Il permet de découvrir le principe des instructions, propres à la programmation par le biais du déplacement des robots par une suite d'instruction. L’idée est de matérialiser ce jeu sous une forme vidéoludique afin de donner à celui-ci une forme attrayante.

La console a été designée spécifiquement pour le jeu vidéo. Elle est composée d'un stick de contrôle des mouvements, d'un bouton rattaché à chaque personnage et d'un bouton pour lancer l’action. L’interface HUD du jeu permet quant à elle de voir l’objectif du niveau ainsi que la séquence rentrée.

Afin de pouvoir utiliser la console avec un groupe de plusieurs élèves, une version analogique du jeu est donné à chaque groupe. Celle-ci est une planche dans lequelle viennent s'encastrer les actions du jeu afin de pouvoir formuler une proposition de réponse avant de passer sur la console.
