---
  layout: project
  weight: 80
  title: "Latroncule"
  year: "2020"
  description: "Fabrication de l'ancêtre du jeu des échecs avec les machines du Fablab"
  important: false
  maker: true
  designer: true
  tags: [Jeu , Fabrication ]
  collaboration: []
  chapeau: "→ Projet de valorisation des possibilités offertes par les machines du Fablab"
  liens: ""
  imagesFolder: latroncules
  thumbnail: LatronculesPhoto01.png
  image:
   - url: LatronculesPhoto01.png
     caption: 'Une partie de Latroncule en cours'
     size: 'big'
   - url: LatronculesPhoto05.png
     caption: 'Les règles du jeu'
     size: small
   - url: LatronculesPhoto02.png
     caption: 'Les règles du jeu'
     size: small
   - url: LatronculesSketch03.png
     caption: 'Esquisses de recherche sur les personnages'
     size: medium
   - url: LatronculesPhoto04.png
     caption: 'Le plateau gravé du Latroncules'
     size: small
---

Le [latroncule](https://fr.wikipedia.org/wiki/Latroncules) est à l'origine un jeu datant du IIIe siècle avant J.-C. Il a été décidé de reproduire ce jeu afin d'en faire un exemple des possibilités offertes par les machines du Fablab.

Ce projet a été réalisé grâce à la fraiseuse, pour le boîtier extérieur, la découpeuse laser pour le plateau et les règles, l'imprimante 3D pour les pions, l'établi pour la découpe des pions et le plotter de découpe pour les visages des pions.

C'est un projet qui permet de faire la médiation du lieu en montrant avec un exemple concret l'usage du matériel.
