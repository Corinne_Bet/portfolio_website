---
  layout: project
  weight: 100
  title: "Affiche Ziklibrenbib 2023"
  year: "2023"
  description: "Réalisation de l'affiche de l'élection Ziklibrenbib 2023"
  important: false
  maker: false
  designer: true
  tags: [Illustration, Design]
  collaboration: []
  chapeau: "→ Réalisation de l'affiche et de la couverture du CD issus de l'élection Ziklibrenbib 2023"
  liens:
   - nom: Le site de l'événement
     lien: https://election.ziklibrenbib.fr/
  imagesFolder: ziklibrenbib2023
  thumbnail: ziklibrenbib202302.png
  image:
   - url: ziklibrenbib202303.png
     caption: "L'affiche de l'événement"
     size: 'big'
   - url: ziklibrenbib202301.png
     caption: "Couverture avant et arrière du CD édité pour l'événement"
     size: 'big'
   - url: ziklibrenbib202306.png
     caption: "Esquisse de recherche"
     size: 'big'
   - url: ziklibrenbib202305.png
     caption: "Recherche autour de la forme de la pédale de musique"
     size: 'big'
   - url: ziklibrenbib202304.png
     caption: "Proposition d'une machine fansmagorique de création de morceau libre de droit"
     size: 'big'
---
J'ai été invité à réaliser l'affiche et la couverture du CD de l'élection du titre de l'année de Ziklibrenbib. Un événement participatif qui récompense des morceaux de musiques libres de droits.

J'ai eu comme première intuition, l'idée d'une machine fantasmagorique qui permet à la fois de voter grâce à un CD mais aussi de transformer ce CD en un morceau libre de droit.

Je suis d'abord partie sur une conception en 3D grâce au logiciel MagicaCSG. Le problème est que cette première idée était en réalité deux idées qui ne faisaient pas sens ensemble. Dans une volonté de simplifier mon idée d'origine, je suis repartie sur de l'illustration et sur le principe d'Urne à voter avec des CDs.

Retrouvez l'ensemble de l'événement et les morceaux sélectionnés sur le site [Ziklibrenbib](https://ziklibrenbib.fr/). Découvrez aussi le super travail et la grande sélection de morceaux réalisée par l'équipe de Ziklibrenbib au quotidien.