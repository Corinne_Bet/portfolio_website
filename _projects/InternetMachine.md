---
  layout: project
  weight: 100
  title: "Internet Machine"
  year: "2020"
  description: "Conception et fabrication d'une escape box modulaire sur le thème des Fablabs"
  important: true
  maker: true
  designer: true
  tags: [Jeu , Fabrication, Électronique, Design, Programmation]
  collaboration:
  chapeau: "→ Dans l'optique de la découverte du Fablab, j'ai conçu et fabriqué une escape box dont chaque énigme est rattachée à une thématique phare des Fablabs"
  liens:
   - nom: Fichier
     lien: https://cad.onshape.com/documents/e56eb810007e45e846084cb9/w/34b69f508fbed268024fba34/e/31cf06a25fd0236c901bdc19
  imagesFolder: internetMachine
  thumbnail: InternetMachinePhoto01.png
  image:
   - url: InternetMachinePhoto02.png
     caption: "L'escape box se présente sous la forme d'un cube multi-facettes"
     size: 'big'
   - url: InternetMachineSketch01.png
     caption: "Esquisse de recherche sur le design de l'escape box"
     size: 'medium'
   - url: InternetMachinePhoto04.png
     caption: "Écran d'accueil de l'escape box"
     size: 'medium'
   - url: InternetMachinePhoto06.png
     caption: "Face électronique"
     size: 'medium'
   - url: InternetMachinePhoto05.png
     caption: "Face documentation"
     size: 'medium'
   - url: InternetMachineSketch03.png
     caption: "Schéma de principe de la modularité des faces"
     size: 'medium'
---

"La machine à Internet est en train de tomber en panne, vous avez 45 minutes pour la sauver et ainsi sauver le monde (virtuel) !"

L'escape box "Internet Machine" est un jeu qui se décline en 4 faces, chacune étant une énigme en lien avec la thématique des Fablabs : l'électronique, la programmation, la fabrication et la documentation. Afin de résoudre ces énigmes, les participants devront mobiliser et découvrir des connaissances en lien avec ces thématiques.

Le projet a été designé dans l'optique d'être reproduit et modifié. Pour cela, chaque panneau, qui correspond à une énigme peut être changé individuellement. L'électronique à l'intérieur est à base du système de connectique [grove](https://www.rs-online.com/designspark/a-first-look-at-the-grove-starter-kit-for-arduino-fr), système de branchement simplifié. Enfin, le boîtier est en grande partie réalisé à la découpeuse laser, machine facilement accessible en Fablab ou Makerspace.

Ce projet est partagé en open-source.
