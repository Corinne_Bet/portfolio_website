---
  layout: project
  weight: 65
  title: "Prix BD Dans ta bulle !"
  year: "2022"
  description: "Identité graphique du prix BD Dans ta bulle !"
  important: false
  maker: false
  designer: true
  tags: [Graphisme , Design]
  collaboration: []
  chapeau: "→ À l'occasion de la première édition du Prix BD Dans Ta Bulle ! d'Argentan, j'ai réalisé l'identité graphique de l'événement"
  liens: 
  imagesFolder: dansTaBulle
  thumbnail: dansTaBulle02.png
  image:
   - url: dansTaBulle02.png
     caption: "Le logotype de l'événement, incarnant de manière figurative le nom de ce dernier"
     size: 'medium'
   - url: dansTaBulle01.png
     caption: "L'affiche finale du prix Dans Ta Bulle !"
     size: 'medium'
   - url: dansTaBulle04.png
     caption: "Sticker pour identifier les BD sélectionnées pour le prix"
     size: 'medium'
   - url: dansTaBulle03.png
     caption: "Exemple d'apposition du sticker sur une BD sélectionnée"
     size: 'medium'
   - url: dansTaBulleGIF.gif
     caption: "Une version animée du logotype pour introductions des vidéos autour de l'événement"
     size: 'medium' 
---

En 2022, la médiathèque d'Argentan et le collège François Truffaut se sont associés pour créer le Prix BD "Dans ta bulle !". Afin d'identifier l'événement, il fallait créer un logotype ainsi qu'une affiche d'annonce. 

J'ai exploré en ce sens plusieurs pistes, sur le coté irrévérencieux du nom et sur son sens figuratif. Des propositions, une a été retenu, jouant des codes graphique d'un label évoquant l'univers des labels de musiques underground. Ce logo joue de la forme du phylactère comme évocation des codes graphiques de la BD mais aussi comme d'une forme d'ouverture de laquelle sort ou se cache un personnage.

Cette dernière idée sera d'autant plus appuyé dans l'animation du logo.

