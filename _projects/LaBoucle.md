---
  layout: project
  weight: 93
  title: "La Boucle"
  year: "2023"
  description: "Une installation particpative autour de l'animation"
  important: false
  maker: true
  designer: true
  tags: [Médiation , Fabrication, Design, Installation, Raspberry pi]
  collaboration: []
  chapeau: "→ Création d'une installation participative pour l'exposition 'Bravo Émile' de la médiathèque d'Argentan. Les visisteurs sont invités à dessiner une image qui mise à la suite des autres créée une animation de marche."
  liens:
   - nom: Programme
     lien: https://gitlab.com/Corinne_Bet/la-boucle
  imagesFolder: laBoucle
  thumbnail: laBoucle01.png
  image:
   - url: LaBoucle.gif
     caption: "L'animation finale de la Boucle composée des dessins de tous les participants"
     size: 'big'
   - url: laBoucle01.png
     caption: "l'espace de l'installation avec une table pour dessiner, un scanner et un écran pour voir l'avencement du projet en direct"
     size: 'big'
   - url: laBoucle04.png
     caption: "Les visiteurs étaient invités à utiliser un gabarit de pose afin d'assurer la cohérence entre les différents dessins"
     size: 'medium'
   - url: laBoucle05.png
     caption: 'Le parcours utilisateur était séquencé afin de permettre son autonomie'
     size: 'medium'
   - url: laBoucle04.png
     caption: 'Le parcours utilisateur était séquencé afin de permettre son autonomie'
     size: 'medium'
   - url: laBoucle03.png
     caption: "Le scan et l'ajout des dessins de manière cohérent dans la séquence était automatisé grâce à un raspberry pi"
     size: 'medium'
---

Dans le cadre de l'exposition, à la médiathèque d'Argentan, sur l'illustrateur et auteur de bandes dessinées, Émile Bravo, j'ai réalisé une installation participative à destination des visiteurs.

Ces derniers pouvaient prendre une feuille et dessiner dessus un personnage en suivant le gabarit fournit. L'ensemble des dessins ont formé à la fin, une boucle d'animation d'un personnage en train de marcher. Un peu plus de 100 dessins ont été réalisés.

L'installation était composée d'un espace de travail, d'un écran pour voir La Boucle en cours, et d'un scanner. Le tout était interfacé grâce à un raspberry pi qui permettait d'automatisé par une simple pression sur un bouton, le lancement du scan, le recadrage du dessin scanné et son insertion de manière cohérente dans La Boucle.