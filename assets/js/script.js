var ua = navigator.userAgent.toLowerCase();
var isMobile = ua.match(/mobile/i);


window.onload = function() {
    document.getElementById('burgerMenu').addEventListener("click", function(){
      document.getElementsByTagName("nav")[0].style.display = 'flex';
    });
    document.getElementById('closeNav').addEventListener("click", function(){
      document.getElementsByTagName("nav")[0].style.display = 'none';

});



////////////////////////////LET APPEAR NAME PROJECT ON SMARTPHONE////////////////////////
if(isMobile){
  //alert('vous êtes sur smartpone')
    window.addEventListener('scroll', (event) => {
      if(document.body.scrollTop <= 100){
        document.getElementById("scrollTOp").innerHTML ="↓";
      } else{
        document.getElementById("scrollTOp").innerHTML ="↑";
      }
      targetMiddleElement();
    });

    function targetMiddleElement(){
      var list = document.getElementsByClassName('gallery')[0].getElementsByTagName('li');
      for( var i =0 ; i < list.length ; i++){
        var item = list[i];
        var elemTop = item.getBoundingClientRect().top;
        if(elemTop < window.screen.height   &&  elemTop > window.screen.height/4){
          item.getElementsByClassName('legende')[0].style.opacity = 1;
          item.getElementsByClassName('overlay')[0].style.opacity = 0;
        } else {
          item.getElementsByClassName('legende')[0].style.opacity = 0;
          item.getElementsByClassName('overlay')[0].style.opacity = 1;
        }
        if(i == list.length){
          item.style.margin-bottom = "10vh"
        }
      }
    }
    }
}
