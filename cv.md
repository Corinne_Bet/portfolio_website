---
layout: cv
title: curriculum vitae
permalink: /cv/

Experiences:
 - date: 2021 - Aujourd'hui
   localisation: Médiathèque François Mitterand, Argentan
   intitule: Fabmanager & médiateur Numérique. Médiation numérique ; Formation informatique ; Programmation et animation de projets pour divers publics ; Formations aux usagers ; Maintenance de premier niveau des machines 
 - date: 2023
   localisation: Lycée polyvalent Mézeray Gabriel, Argentan
   intitule: Membre de jury pour la soutenance de projet des élèves de DNMADe objet.  
 - date: 2018 - 2021
   localisation: Micro-Folie/Les Franciscaines, Deauville
   intitule: Fabmanager du Fablab de Deauville. Développement de projets ; Développement, programmation et animation de projets pour divers publics ;  Formations aux usagers ; Maintenance de premier niveau des machines ; Équipement et installation du Fablab ; Participation à la sélection  de documents de la médiathèque en lien avec le Fablab.  
 - date: 2018
   localisation: Mahrora Village, Inde
   intitule: Volontariat de 2 semaines (WWOOF) à la ferme Geeli Mitti.
 - date: 2017-2018
   localisation: Fablab du Dôme, Caen
   intitule: Service civique de 8 mois au Fablab du Dôme de Caen. Formations usagers ; Développement de projets ; Participation à la programmation.
 - date: 2016
   localisation: Musée de la découverte, Paris
   intitule: Participation à Muséomix Paris en tant que facilitateur. Réflexion et accompagnement technique des groupes.
 - date: 2016
   localisation: Agence Chattermark, Cherbourg
   intitule: Stage de 4 semaines dans l'agence de design graphique Chattermark. Recherches et réflexions sur des projets.
 - date: 2016
   localisation: École supérieure des arts et industries graphiques Estienne, Paris
   intitule: Création de l'association Les codeurs à la sauvette. Association d'animation d’ateliers d’initiation à la culture et aux  techniques numériques à destination des élèves de l’école Estienne.
 - date: 2016
   localisation: Mediapart, Paris
   intitule: Participation à la Gamejam autour de la thématique des élections de 2017, organisée dans les locaux de Mediapart. Réalisation du jeu Murmurations.
 - date: 2016
   localisation: Fablab de la Waag Society, Amsterdam
   intitule: Stage de 12 semaines au FabLab d'Amsterdam. Formations aux machines ; Prototypage d'une nouvelle signalétique ; Participation à la programmation.
 - date: 2014
   localisation: CH1, Cherbourg
   intitule: Stage de 7 semaines dans l'agence de webdesign CH1. Développement d'un thème wordpress ; Refonte graphique.
   
Formations:
 - date: 2015-2017
   localisation: École supérieure des arts et industries graphiques Estienne, Paris
   intitule: Diplôme Supérieur en Arts Appliqués en Design et création numérique.
 - date: 2013-2015
   localisation: Lycée Bréquigny, Rennes
   intitule: Brevet de technicien supérieur en desgin graphique option communication et médias numériques.
 - date: 2012-2013
   localisation: Lycée Laplace, Caen
   intitule: Mise à Niveau en Arts Appliqués.
 - date: 2012
   localisation: Lycée Jean-François Millet, Cherbourg
   intitule: Obtention du BAC Économique et social spécialité mathématiques option Arts Plastiques.

Langues:
 - nationnalite: Anglais
   niveau: Lu et parlé
 - nationnalite: Espagnol
   niveau: Niveau terminale

Competences:
 - categorie: Fabrication
   logiciels: Découpeuse Laser, Imprimante 3D, Fraiseuse, Plotter de découpe, Découpeuse à fil chaud, Brodeuse numérique, Outils électroportatifs
 - categorie: Modélisation 3D
   logiciels: Onshape, Fusion 360, Blender, Magica CSG, Tinkercad
 - categorie: Jeu vidéo
   logiciels: Scratch, Pico-8, Tic-80, Godot, Unity
 - categorie: Programmation
   logiciels: Html, Css, Javascript, Processing, Arduino, P5.js, Jekyll, Kodular
 - categorie: CAO
   logiciels: Photoshop, After Effects, Illustrator, InDesign, Premiere Pro, Scribus, Gimp, Inkscape, Figma, OBS

Interets:
 - sujet: Game Design
 - sujet: Bande dessinée et illustration
 - sujet: Écrits et recherches dans les champs du design et du numérique
 - sujet: Arts numériques
 - sujet: Jeux de société et jeux vidéos
---
